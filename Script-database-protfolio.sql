
CREATE SEQUENCE public.profil_id_seq;

CREATE TABLE public.profil (
                id INTEGER NOT NULL DEFAULT nextval('public.profil_id_seq'),
                last_name VARCHAR NOT NULL,
                first_name VARCHAR NOT NULL,
                adress VARCHAR NOT NULL,
                date_of_birth TIMESTAMP NOT NULL,
                phone_number VARCHAR NOT NULL,
                email VARCHAR NOT NULL,
                CONSTRAINT profil_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.profil_id_seq OWNED BY public.profil.id;

CREATE SEQUENCE public.conference_id_seq;

CREATE TABLE public.conference (
                id INTEGER NOT NULL DEFAULT nextval('public.conference_id_seq'),
                name VARCHAR NOT NULL,
                summarize VARCHAR NOT NULL,
                profil_id INTEGER NOT NULL,
                CONSTRAINT conference_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.conference_id_seq OWNED BY public.conference.id;

CREATE SEQUENCE public.video_id_seq;

CREATE TABLE public.video (
                id INTEGER NOT NULL DEFAULT nextval('public.video_id_seq'),
                title VARCHAR NOT NULL,
                video_url VARCHAR NOT NULL,
                conference_id INTEGER NOT NULL,
                CONSTRAINT video_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.video_id_seq OWNED BY public.video.id;

CREATE SEQUENCE public.point_of_interest_id_seq;

CREATE TABLE public.point_of_interest (
                id INTEGER NOT NULL DEFAULT nextval('public.point_of_interest_id_seq'),
                name VARCHAR NOT NULL,
                paragraph VARCHAR,
                profil_id INTEGER NOT NULL,
                CONSTRAINT point_of_interest_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.point_of_interest_id_seq OWNED BY public.point_of_interest.id;

CREATE SEQUENCE public.experience_id_seq;

CREATE TABLE public.experience (
                id INTEGER NOT NULL DEFAULT nextval('public.experience_id_seq'),
                title VARCHAR NOT NULL,
                company VARCHAR NOT NULL,
                commencement_date TIMESTAMP NOT NULL,
                stop_date TIMESTAMP NOT NULL,
                city VARCHAR NOT NULL,
                paragraph VARCHAR,
                profil_id INTEGER NOT NULL,
                CONSTRAINT experience_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.experience_id_seq OWNED BY public.experience.id;

CREATE SEQUENCE public.competence_id_seq;

CREATE TABLE public.competence (
                id INTEGER NOT NULL DEFAULT nextval('public.competence_id_seq'),
                name VARCHAR NOT NULL,
                level INTEGER NOT NULL,
                number_of_practice_months INTEGER NOT NULL,
                profil_id INTEGER NOT NULL,
                CONSTRAINT competence_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.competence_id_seq OWNED BY public.competence.id;

CREATE SEQUENCE public.diploma_id_seq;

CREATE TABLE public.diploma (
                id INTEGER NOT NULL DEFAULT nextval('public.diploma_id_seq'),
                title VARCHAR NOT NULL,
                obtaining TIMESTAMP NOT NULL,
                school VARCHAR NOT NULL,
                city VARCHAR NOT NULL,
                paragraph VARCHAR,
                profil_id INTEGER NOT NULL,
                CONSTRAINT diploma_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.diploma_id_seq OWNED BY public.diploma.id;

CREATE SEQUENCE public.social_network_id_seq;

CREATE TABLE public.social_network (
                id INTEGER NOT NULL DEFAULT nextval('public.social_network_id_seq'),
                name VARCHAR NOT NULL,
                url VARCHAR NOT NULL,
                profil_id INTEGER NOT NULL,
                CONSTRAINT social_network_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.social_network_id_seq OWNED BY public.social_network.id;

CREATE SEQUENCE public.proof_of_concept_id_seq;

CREATE TABLE public.proof_of_concept (
                id INTEGER NOT NULL DEFAULT nextval('public.proof_of_concept_id_seq'),
                name VARCHAR NOT NULL,
                publication TIMESTAMP NOT NULL,
                summarize VARCHAR NOT NULL,
                CONSTRAINT proof_of_concept_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.proof_of_concept_id_seq OWNED BY public.proof_of_concept.id;

CREATE SEQUENCE public.tag_id_seq;

CREATE TABLE public.tag (
                id INTEGER NOT NULL DEFAULT nextval('public.tag_id_seq'),
                name VARCHAR NOT NULL,
                tag_path VARCHAR NOT NULL,
                CONSTRAINT tag_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tag_id_seq OWNED BY public.tag.id;

CREATE SEQUENCE public.tag_pointofinterest_id_seq;

CREATE TABLE public.tag_pointofinterest (
                id INTEGER NOT NULL DEFAULT nextval('public.tag_pointofinterest_id_seq'),
                tag_id INTEGER NOT NULL,
                point_of_interest_id INTEGER NOT NULL,
                CONSTRAINT tag_pointofinterest_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tag_pointofinterest_id_seq OWNED BY public.tag_pointofinterest.id;

CREATE SEQUENCE public.image_id_seq;

CREATE TABLE public.image (
                id INTEGER NOT NULL DEFAULT nextval('public.image_id_seq'),
                name VARCHAR NOT NULL,
                image_path VARCHAR NOT NULL,
                CONSTRAINT image_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_id_seq OWNED BY public.image.id;

CREATE SEQUENCE public.image_diploma_id_seq;

CREATE TABLE public.image_diploma (
                id INTEGER NOT NULL DEFAULT nextval('public.image_diploma_id_seq'),
                image_id INTEGER NOT NULL,
                diploma_id INTEGER NOT NULL,
                CONSTRAINT image_diploma_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_diploma_id_seq OWNED BY public.image_diploma.id;

CREATE SEQUENCE public.image_conference_id_seq;

CREATE TABLE public.image_conference (
                id INTEGER NOT NULL DEFAULT nextval('public.image_conference_id_seq'),
                image_id INTEGER NOT NULL,
                conference_id INTEGER NOT NULL,
                CONSTRAINT image_conference_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_conference_id_seq OWNED BY public.image_conference.id;

CREATE SEQUENCE public.image_experience_id_seq;

CREATE TABLE public.image_experience (
                id INTEGER NOT NULL DEFAULT nextval('public.image_experience_id_seq'),
                image_id INTEGER NOT NULL,
                experience_id INTEGER NOT NULL,
                CONSTRAINT image_experience_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_experience_id_seq OWNED BY public.image_experience.id;

CREATE SEQUENCE public.image_proofofconcept_id_seq;

CREATE TABLE public.image_proofofconcept (
                id INTEGER NOT NULL DEFAULT nextval('public.image_proofofconcept_id_seq'),
                image_id INTEGER NOT NULL,
                proof_of_concept_id INTEGER NOT NULL,
                CONSTRAINT image_proofofconcept_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_proofofconcept_id_seq OWNED BY public.image_proofofconcept.id;

CREATE SEQUENCE public.resource_id_seq;

CREATE TABLE public.resource (
                id INTEGER NOT NULL DEFAULT nextval('public.resource_id_seq'),
                name VARCHAR NOT NULL,
                url VARCHAR NOT NULL,
                CONSTRAINT resource_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.resource_id_seq OWNED BY public.resource.id;

CREATE SEQUENCE public.resource_proofofconcept_id_seq;

CREATE TABLE public.resource_proofofconcept (
                id INTEGER NOT NULL DEFAULT nextval('public.resource_proofofconcept_id_seq'),
                resource_id INTEGER NOT NULL,
                proof_of_concept_id INTEGER NOT NULL,
                CONSTRAINT resource_proofofconcept_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.resource_proofofconcept_id_seq OWNED BY public.resource_proofofconcept.id;

CREATE UNIQUE INDEX resource_proofofconcept_idx
 ON public.resource_proofofconcept
 ( resource_id, proof_of_concept_id );

CREATE SEQUENCE public.project_id_seq;

CREATE TABLE public.project (
                id INTEGER NOT NULL DEFAULT nextval('public.project_id_seq'),
                name VARCHAR NOT NULL,
                publication TIMESTAMP NOT NULL,
                summarize VARCHAR NOT NULL,
                CONSTRAINT project_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.project_id_seq OWNED BY public.project.id;

CREATE SEQUENCE public.image_project_id_seq;

CREATE TABLE public.image_project (
                id INTEGER NOT NULL DEFAULT nextval('public.image_project_id_seq'),
                image_id INTEGER NOT NULL,
                project_id INTEGER NOT NULL,
                CONSTRAINT image_project_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_project_id_seq OWNED BY public.image_project.id;

CREATE SEQUENCE public.resource_project_id_seq;

CREATE TABLE public.resource_project (
                id INTEGER NOT NULL DEFAULT nextval('public.resource_project_id_seq'),
                resource_id INTEGER NOT NULL,
                project_id INTEGER NOT NULL,
                CONSTRAINT resource_project_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.resource_project_id_seq OWNED BY public.resource_project.id;

CREATE UNIQUE INDEX resource_project_idx
 ON public.resource_project
 ( resource_id, project_id );

CREATE SEQUENCE public.article_id_seq;

CREATE TABLE public.article (
                id INTEGER NOT NULL DEFAULT nextval('public.article_id_seq'),
                title VARCHAR,
                position INTEGER NOT NULL,
                project_id INTEGER,
                proof_of_concept_id INTEGER,
                CONSTRAINT article_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.article_id_seq OWNED BY public.article.id;

CREATE SEQUENCE public.tag_article_id_seq;

CREATE TABLE public.tag_article (
                id INTEGER NOT NULL DEFAULT nextval('public.tag_article_id_seq'),
                tag_id INTEGER NOT NULL,
                article_id INTEGER NOT NULL,
                CONSTRAINT tag_article_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tag_article_id_seq OWNED BY public.tag_article.id;

CREATE SEQUENCE public.resource_article_id_seq;

CREATE TABLE public.resource_article (
                id INTEGER NOT NULL DEFAULT nextval('public.resource_article_id_seq'),
                resource_id INTEGER NOT NULL,
                article_id INTEGER NOT NULL,
                CONSTRAINT resource_article_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.resource_article_id_seq OWNED BY public.resource_article.id;

CREATE SEQUENCE public.paragraph_id_seq;

CREATE TABLE public.paragraph (
                id INTEGER NOT NULL DEFAULT nextval('public.paragraph_id_seq'),
                subtitle VARCHAR,
                paragraph VARCHAR NOT NULL,
                position INTEGER NOT NULL,
                article_id INTEGER NOT NULL,
                CONSTRAINT paragraph_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.paragraph_id_seq OWNED BY public.paragraph.id;

CREATE SEQUENCE public.image_paragraph_id_seq;

CREATE TABLE public.image_paragraph (
                id INTEGER NOT NULL DEFAULT nextval('public.image_paragraph_id_seq'),
                image_id INTEGER NOT NULL,
                paragraph_id INTEGER NOT NULL,
                CONSTRAINT image_paragraph_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.image_paragraph_id_seq OWNED BY public.image_paragraph.id;

ALTER TABLE public.social_network ADD CONSTRAINT profil_social_network_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.diploma ADD CONSTRAINT profil_diploma_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.competence ADD CONSTRAINT profil_competence_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.experience ADD CONSTRAINT profil_experience_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.point_of_interest ADD CONSTRAINT profil_point_of_interest_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.conference ADD CONSTRAINT profil_conference_fk
FOREIGN KEY (profil_id)
REFERENCES public.profil (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.video ADD CONSTRAINT conference_video_fk
FOREIGN KEY (conference_id)
REFERENCES public.conference (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.image_conference ADD CONSTRAINT conference_image_conference_fk
FOREIGN KEY (conference_id)
REFERENCES public.conference (id)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tag_pointofinterest ADD CONSTRAINT point_of_interest_tag_pointofinterest_fk
FOREIGN KEY (point_of_interest_id)
REFERENCES public.point_of_interest (id)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_experience ADD CONSTRAINT experience_image_experience_fk
FOREIGN KEY (experience_id)
REFERENCES public.experience (id)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_diploma ADD CONSTRAINT diploma_image_diploma_fk
FOREIGN KEY (diploma_id)
REFERENCES public.diploma (id)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.article ADD CONSTRAINT proof_of_concept_article_fk
FOREIGN KEY (proof_of_concept_id)
REFERENCES public.proof_of_concept (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.resource_proofofconcept ADD CONSTRAINT proof_of_concept_resource_proofofconcept_fk
FOREIGN KEY (proof_of_concept_id)
REFERENCES public.proof_of_concept (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.image_proofofconcept ADD CONSTRAINT proof_of_concept_image_proofofconcept_fk
FOREIGN KEY (proof_of_concept_id)
REFERENCES public.proof_of_concept (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tag_article ADD CONSTRAINT tag_tag_article_fk
FOREIGN KEY (tag_id)
REFERENCES public.tag (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tag_pointofinterest ADD CONSTRAINT tag_tag_pointofinterest_fk
FOREIGN KEY (tag_id)
REFERENCES public.tag (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_project ADD CONSTRAINT image_image_project_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_proofofconcept ADD CONSTRAINT image_image_proofofconcept_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_experience ADD CONSTRAINT image_image_experience_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_conference ADD CONSTRAINT image_image_conference_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_diploma ADD CONSTRAINT image_image_diploma_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.image_paragraph ADD CONSTRAINT image_image_paragraph_fk
FOREIGN KEY (image_id)
REFERENCES public.image (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.resource_project ADD CONSTRAINT resource_resource_project_fk
FOREIGN KEY (resource_id)
REFERENCES public.resource (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.resource_article ADD CONSTRAINT resource_resource_article_fk
FOREIGN KEY (resource_id)
REFERENCES public.resource (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.resource_proofofconcept ADD CONSTRAINT resource_resource_proofofconcept_fk
FOREIGN KEY (resource_id)
REFERENCES public.resource (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.article ADD CONSTRAINT project_article_fk
FOREIGN KEY (project_id)
REFERENCES public.project (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.resource_project ADD CONSTRAINT project_resource_project_fk
FOREIGN KEY (project_id)
REFERENCES public.project (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.image_project ADD CONSTRAINT project_image_project_fk
FOREIGN KEY (project_id)
REFERENCES public.project (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.paragraph ADD CONSTRAINT article_paragraph_fk
FOREIGN KEY (article_id)
REFERENCES public.article (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.resource_article ADD CONSTRAINT article_resource_article_fk
FOREIGN KEY (article_id)
REFERENCES public.article (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.tag_article ADD CONSTRAINT article_tag_article_fk
FOREIGN KEY (article_id)
REFERENCES public.article (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.image_paragraph ADD CONSTRAINT paragraph_image_paragraph_fk
FOREIGN KEY (paragraph_id)
REFERENCES public.paragraph (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;
